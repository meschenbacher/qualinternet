package main

import (
	"net"
)

func LookupHostOnePerFamily(target string) ([]string, error) {
	ips, err := net.LookupHost(target)
	if err != nil {
		return nil, err
	}
	res := make([]string, 2)
	var foundv4, foundv6 bool
	for _, ip := range ips {
		if !foundv4 && len(ip) == net.IPv4len {
			res = append(res, ip)
			foundv4 = true
		} else if !foundv6 {
			res = append(res, ip)
			foundv6 = true
		}
	}
	return ips, nil
}
