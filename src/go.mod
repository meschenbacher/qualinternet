module gitlab.com/meschenbacher/qualinternet

go 1.15

require (
	crawshaw.io/sqlite v0.3.2
	github.com/lib/pq v1.9.0
	github.com/shopspring/decimal v1.2.0
	github.com/meschenbacher/speedtest-go v1.1.1-0.20210131114028-6081c53414cb
	gopkg.in/ini.v1 v1.62.0
)
