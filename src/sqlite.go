package main

import (
	"crawshaw.io/sqlite/sqlitex"
	"encoding/json"
	st "github.com/meschenbacher/speedtest-go"
	"gopkg.in/ini.v1"
	"log"
)

type SqliteOutputter struct {
	q      *Qualinternet
	dbpool *sqlitex.Pool
}

func NewSqliteOutputter(q *Qualinternet, section *ini.Section) *SqliteOutputter {
	db_path := section.Key("path").Value()
	dbpool, err := sqlitex.Open(db_path, 0, 10)
	if err != nil {
		log.Fatalf("Failed to open database: %v", err)
	}
	db := dbpool.Get(nil)
	defer dbpool.Put(db)

	err = sqlitex.Exec(db, `
CREATE TABLE IF NOT EXISTS pingresult (
	id INTEGER PRIMARY KEY,
	prober varchar(255) not null,
	timestamp timestamp not null default current_timestamp,
	target varchar(255) not null,
	ip varchar(50),
	rx int,
	tx int,
	packetloss decimal(6,3),
	min decimal(8,3),
	avg decimal(8,3),
	max decimal(8,3),
	mdev decimal(8,3)
)`, nil)
	if err != nil {
		log.Fatalf("Failed to execute statement: %v", err)
	}

	err = sqlitex.Exec(db, `
CREATE TABLE IF NOT EXISTS mtrresult (
	id INTEGER PRIMARY KEY,
	prober varchar(255) not null,
	timestamp timestamp not null default current_timestamp,
	target varchar(255) not null,
	ip varchar(50),
	mtr text
)`, nil)
	if err != nil {
		log.Fatalf("Failed to exec statement: %v", err)
	}

	err = sqlitex.Exec(db, `
CREATE TABLE IF NOT EXISTS speedtest_agg (
	id INTEGER PRIMARY KEY,
	prober varchar(255) not null,
	timestamp timestamp not null default current_timestamp,
	user jsonb not null
)`, nil)
	if err != nil {
		log.Fatalf("Failed to exec statement: %v", err)
	}

	err = sqlitex.Exec(db, `
CREATE TABLE IF NOT EXISTS speedtest (
	id INTEGER PRIMARY KEY,
	agg int not null references speedtest_agg(id) on delete cascade,
	timestamp timestamp not null default current_timestamp,
	target jsonb not null
)`, nil)
	if err != nil {
		log.Fatalf("Failed to exec statement: %v", err)
	}

	return &SqliteOutputter{q: q, dbpool: dbpool}
}

func (o *SqliteOutputter) PingResult(r *pingResult) {
	db := o.dbpool.Get(nil)
	defer o.dbpool.Put(db)
	err := sqlitex.Exec(db, "INSERT INTO pingresult (prober, target, ip, rx, tx, packetloss, min, avg, max, mdev) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", nil, o.q.probe_name, r.target, r.ip, r.rx, r.tx, r.packetloss, r.min, r.avg, r.max, r.mdev)
	if err != nil {
		log.Fatalf("failed to execute statement %v\n", err)
	}
}
func (o *SqliteOutputter) MtrResult(r *mtrResult) {
	db := o.dbpool.Get(nil)
	defer o.dbpool.Put(db)
	err := sqlitex.Exec(db, "INSERT INTO mtrresult (prober, target, ip, mtr) values (?, ?, ?, ?)", nil, o.q.probe_name, r.target, r.ip, r.mtr)
	if err != nil {
		log.Fatalf("failed to execute statement %v\n", err)
	}
}

func (o *SqliteOutputter) SpeedtestResult(r *speedtestResult) {
	db := o.dbpool.Get(nil)
	defer o.dbpool.Put(db)
	targetj, err := json.Marshal(r.target)
	if err != nil {
		log.Printf("failed to marshal json %v\n", r)
		return
	}
	err = sqlitex.Exec(db, "INSERT INTO speedtest (agg, target) values ($1, $2)", nil, r.agg, targetj)
	if err != nil {
		log.Printf("failed to execute statement %v\n", err)
	}
}

func (o *SqliteOutputter) VPNResult(r *vpnResult) {
	log.Fatalf("VPNResult is not yet implemented")
}

func (o *SqliteOutputter) InitSpeedtestRun(u *st.User, q *Qualinternet) (int64, error) {
	db := o.dbpool.Get(nil)
	defer o.dbpool.Put(db)
	userj, err := json.Marshal(u)
	if err != nil {
		log.Printf("failed to marshal json %v\n", u)
		return 0, err
	}
	err = sqlitex.Exec(db, "INSERT INTO speedtest_agg (prober, user) values ($1, $2)", nil, o.q.probe_name, userj)
	if err != nil {
		log.Printf("failed to execute statement %v\n", err)
	}
	lid := db.LastInsertRowID()
	return lid, nil
}
