package main

import (
	st "github.com/meschenbacher/speedtest-go"
	"log"
	"sync"
	"time"
)

type speedtestResult struct {
	target *st.Server
	agg    int64
}

func speedtest(q *Qualinternet, o chan speedtestResult, sleep uint, concurrent uint) {
	for {
		go func() {
			user, err := st.FetchUserInfo()
			if err != nil {
				log.Print(err)
				return
			}

			agg, err := q.o.InitSpeedtestRun(user, q)
			if err != nil {
				log.Print(err)
				return
			}

			serverlist, err := st.FetchServerList(user)
			if err != nil {
				log.Print(err)
				return
			}
			targets := serverlist.Servers[:concurrent]

			var wg, wgping, wgupload sync.WaitGroup
			for _, t := range targets {
				// copy t to avoid race condition
				t := t
				wg.Add(1)
				wgping.Add(1)
				wgupload.Add(1)
				go func() {
					defer wg.Done()
					t.PingTest()
					wgping.Done()
					wgping.Wait()
					t.UploadTest(false)
					wgupload.Done()
					wgupload.Wait()
					t.DownloadTest(false)
					r := &speedtestResult{
						agg: agg,
						// user:   user,
						target: t,
					}
					o <- *r
				}()
			}
			wg.Wait()
		}()
		time.Sleep(time.Duration(sleep) * time.Second)
	}
}
