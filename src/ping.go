package main

import (
	"fmt"
	"github.com/shopspring/decimal"
	"log"
	"os/exec"
	"regexp"
	"strconv"
	"time"
)

type pingResult struct {
	target, ip                      string
	rx, tx                          int64
	packetloss, min, avg, max, mdev decimal.Decimal
}

func pingIP(q *Qualinternet, target string, ip string, count uint) {
	stdout, err := exec.Command("ping", "-c", fmt.Sprint(count), ip).Output()
	if err != nil {
		log.Printf("ping for ip %s failed %v", ip, err)
		return
	}
	re := regexp.MustCompile(`(?s)(\d+) packets transmitted, (\d+) received, ([0-9.]+)% packet loss, time \d+ms\n[^\d]+(\d+\.\d+)\/(\d+\.\d+)\/(\d+\.\d+)\/(\d+\.\d+) ms`)
	matches := re.FindStringSubmatch(string(stdout))
	if matches == nil {
		log.Printf("could not match ping output: %s", string(stdout))
		return
	}
	r := &pingResult{}
	r.target = target
	r.ip = ip
	if i, err := strconv.ParseInt(matches[1], 10, 32); err != nil {
		log.Print(err)
	} else {
		r.tx = i
	}
	if i, err := strconv.ParseInt(matches[2], 10, 32); err != nil {
		log.Print(err)
	} else {
		r.rx = i
	}
	if d, err := decimal.NewFromString(matches[3]); err != nil {
		log.Print(err)
	} else {
		r.packetloss = d
	}
	if d, err := decimal.NewFromString(matches[4]); err != nil {
		log.Print(err)
	} else {
		r.min = d
	}
	if d, err := decimal.NewFromString(matches[5]); err != nil {
		log.Print(err)
	} else {
		r.avg = d
	}
	if d, err := decimal.NewFromString(matches[6]); err != nil {
		log.Print(err)
	} else {
		r.max = d
	}
	if d, err := decimal.NewFromString(matches[7]); err != nil {
		log.Print(err)
	} else {
		r.mdev = d
	}
	q.pingChan <- *r
}

func ping(q *Qualinternet, sleep uint, target string, count uint) {
	for {
		ips, err := LookupHostOnePerFamily(target)
		if err != nil {
			log.Printf("error looking up host %s: %v", target, err)
			return
		}
		for _, ip := range ips {
			go pingIP(q, target, ip, count)
		}
		time.Sleep(time.Duration(sleep) * time.Second)
	}
}
