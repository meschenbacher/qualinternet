package main

type Environment struct {
	os, gwv4, gwv6 string
	resolvers      []string
}

func detectEnvironment() *Environment {
	return &Environment{}
}
