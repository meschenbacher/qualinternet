package main

import (
	// "encoding/json"
	"fmt"
	st "github.com/meschenbacher/speedtest-go"
	"gopkg.in/ini.v1"
	// "log"
)

type ConsoleOutputter struct{}

func (o *ConsoleOutputter) PingResult(r *pingResult) {
	fmt.Printf("--- %s ping statistics (%s) ---\n", r.target, r.ip)
	fmt.Printf("%d packets transmitted, %d received, %s%% packet loss\n", r.tx, r.rx, r.packetloss.String())
	fmt.Printf("rtt min/avg/max/mdev = %s/%s/%s/%s ms\n\n", r.min.String(), r.avg.String(), r.max.String(), r.mdev.String())
}
func (o *ConsoleOutputter) MtrResult(r *mtrResult) {
	fmt.Printf("%v", r.mtr)
}
func (o *ConsoleOutputter) VPNResult(r *vpnResult) {
	fmt.Printf("%v", r)
}
func (o *ConsoleOutputter) InitSpeedtestRun(u *st.User, q *Qualinternet) (int64, error) {
	return 0, nil
}

func (o *ConsoleOutputter) SpeedtestResult(r *speedtestResult) {
	// targetj, err := json.Marshal(r.target)
	// if err != nil {
	// log.Printf("failed to marshal json %v", r)
	// return
	// }
	fmt.Printf("%v, %+v\n", r.agg, r.target)
}

func NewConsoleOutputter(q *Qualinternet, section *ini.Section) *ConsoleOutputter {
	return &ConsoleOutputter{}
}
