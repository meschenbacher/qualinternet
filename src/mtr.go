package main

import (
	"log"
	"os/exec"
	"time"
)

type mtrResult struct {
	target, ip, mtr string
}

func mtr(o chan mtrResult, sleep uint, target string) {
	for {
		ips, err := LookupHostOnePerFamily(target)
		if err != nil {
			log.Printf("error looking up host %s: %v\n", target, err)
		}
		for _, ip := range ips {
			go func(ip string) {
				stdout, err := exec.Command("mtr", "--report", "--report-wide", "--no-dns", ip).Output()
				if err != nil {
					log.Printf("mtr for ip %s failed %v", ip, err)
					return
				}
				r := &mtrResult{target: target, ip: ip, mtr: string(stdout)}
				o <- *r
			}(ip)
		}
		time.Sleep(time.Duration(sleep) * time.Second)
	}
}
