package main

import (
	"database/sql"
	"encoding/json"
	_ "github.com/lib/pq"
	st "github.com/meschenbacher/speedtest-go"
	"gopkg.in/ini.v1"
	"log"
	"time"
)

type PostgresOutputter struct {
	q   *Qualinternet
	con *sql.DB
	dsn string
}

func (o *PostgresOutputter) db() *sql.DB {
	for o.con == nil || o.con.Ping() == nil {
		if o.con != nil && o.con.Ping() == nil {
			o.con.Close()
		}
		con, err := sql.Open("postgres", o.dsn)
		if err != nil {
			log.Printf("Failed to open database: %v", err)
			time.Sleep(1 * time.Second)
		}
		o.con = con
		return con
	}
	return o.con
}

func NewPostgresOutputter(q *Qualinternet, section *ini.Section) *PostgresOutputter {
	dsn := section.Key("dsn").Value()
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatalf("Failed to open database: %v", err)
	}
	_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS pingresult (
	id SERIAL PRIMARY KEY,
	prober varchar(255) not null,
	timestamp timestamp not null default current_timestamp,
	target varchar(255) not null,
	ip varchar(50),
	rx int,
	tx int,
	packetloss decimal(6,3),
	min decimal(8,3),
	avg decimal(8,3),
	max decimal(8,3),
	mdev decimal(8,3)
)`)
	if err != nil {
		log.Fatalf("Failed to execute statement: %v\n", err)
	}

	_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS mtrresult (
	id SERIAL PRIMARY KEY,
	prober varchar(255) not null,
	timestamp timestamp not null default current_timestamp,
	target varchar(255) not null,
	ip varchar(50),
	mtr text
)`)
	if err != nil {
		log.Fatalf("Failed to exec statement: %v", err)
	}

	_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS speedtest_agg (
	id SERIAL PRIMARY KEY,
	prober varchar(255) not null,
	timestamp timestamp not null default current_timestamp,
	"user" jsonb not null
)`)
	if err != nil {
		log.Fatalf("Failed to exec statement: %v", err)
	}

	_, err = db.Exec(`
CREATE TABLE IF NOT EXISTS speedtest (
	id SERIAL PRIMARY KEY,
	agg int not null references speedtest_agg(id) on delete cascade,
	timestamp timestamp not null default current_timestamp,
	target jsonb not null
)`)
	if err != nil {
		log.Fatalf("Failed to exec statement: %v", err)
	}

	return &PostgresOutputter{q: q, con: db, dsn: dsn}
}

func (o *PostgresOutputter) PingResult(r *pingResult) {
	_, err := o.db().Exec("INSERT INTO pingresult (prober, target, ip, rx, tx, packetloss, min, avg, max, mdev) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)", o.q.probe_name, r.target, r.ip, r.rx, r.tx, r.packetloss, r.min, r.avg, r.max, r.mdev)
	if err != nil {
		log.Printf("insert into pingresult error %v", err)
	}
}

func (o *PostgresOutputter) MtrResult(r *mtrResult) {
	_, err := o.db().Exec("INSERT INTO mtrresult (prober, target, ip, mtr) values ($1, $2, $3, $4)", o.q.probe_name, r.target, r.ip, r.mtr)
	if err != nil {
		log.Printf("insert into mtrresult error %v", err)
	}
}

func (o *PostgresOutputter) SpeedtestResult(r *speedtestResult) {
	targetj, err := json.Marshal(r.target)
	if err != nil {
		log.Printf("failed to marshal json %v", r)
		return
	}
	_, err = o.db().Exec(`INSERT INTO speedtest(agg, target) values ($1, $2)`, r.agg, targetj)
	if err != nil {
		log.Printf("failed to execute statement %v", err)
	}
}

func (o *PostgresOutputter) VPNResult(r *vpnResult) {
	log.Fatalf("VPNResult is not yet implemented")
}

func (o *PostgresOutputter) InitSpeedtestRun(u *st.User, q *Qualinternet) (int64, error) {
	userj, err := json.Marshal(u)
	if err != nil {
		return 0, err
	}
	var lid int64
	err = o.db().QueryRow(`INSERT INTO speedtest_agg (prober, "user") values ($1, $2) returning id`, q.probe_name, userj).Scan(&lid)
	if err != nil {
		return 0, err
	}
	return lid, nil
}
