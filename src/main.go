package main

import (
	"fmt"
	st "github.com/meschenbacher/speedtest-go"
	"gopkg.in/ini.v1"
	"log"
	"os"
	"os/exec"
	"sort"
)

type Qualinternet struct {
	probe_name                             string
	ping_every, mtr_every, speedtest_every uint
	cfg                                    *ini.File
	o                                      Outputter
	pingChan                               chan pingResult
	mtrChan                                chan mtrResult
	speedtestChan                          chan speedtestResult
}

type Outputter interface {
	PingResult(*pingResult)
	MtrResult(*mtrResult)
	SpeedtestResult(*speedtestResult)
	VPNResult(*vpnResult)
	InitSpeedtestRun(*st.User, *Qualinternet) (int64, error)
}

func dns(sleep int, target string, dns_resolver string) {
}

func vpn(sleep int, target string) {
}

func checkRequirements() {
	requiredBinaries := []string{"ping", "mtr"}
	failedBinaries := make([]string, 0)
	for _, bin := range requiredBinaries {
		if _, err := exec.LookPath(bin); err != nil {
			failedBinaries = append(failedBinaries, bin)
		}
	}
	if len(failedBinaries) > 0 {
		log.Fatalf("%v does not exist in PATH len %d", failedBinaries, len(failedBinaries))
	}
}

type SortableIniSections []*ini.Section

func (b SortableIniSections) Len() int           { return len(b) }
func (b SortableIniSections) Swap(i, j int)      { b[i], b[j] = b[j], b[i] }
func (b SortableIniSections) Less(i, j int) bool { return b[i].Name() < b[j].Name() }

func start(args []string) {
	if len(os.Args) != 3 {
		usage()
	}
	cfg, err := ini.ShadowLoad(os.Args[2])
	if err != nil {
		log.Fatalf("Fail to read file: %v", err)
	}
	for _, secname := range cfg.SectionStrings() {
		if secname == "speedtest" {
			// log.Printf("%s: %v\n", secname, cfg.Section(secname).Key("type").ValueWithShadows())
		} else {
			// log.Printf("%s: %v\n", secname, cfg.Section(secname).Key("target").ValueWithShadows())
		}
	}

	q := &Qualinternet{}
	general := cfg.Section("general")
	if cfg.Section("general") == nil {
		log.Fatal("exactly one [general] section must be present")
	}

	if general.Haskey("probe_name") {
		q.probe_name = general.Key("probe_name").Value()
	} else {
		q.probe_name, err = os.Hostname()
		if err != nil {
			log.Fatal("probe_name not set and could not determine hostname")
		}
	}
	if key := general.Key("ping_every"); key != nil {
		q.ping_every = key.MustUint(180)
	} else {
		q.ping_every = 180
	}
	if key := general.Key("mtr_every"); key != nil {
		q.mtr_every = key.MustUint(600)
	} else {
		q.mtr_every = 600
	}
	if key := general.Key("speedtest_every"); key != nil {
		q.speedtest_every = key.MustUint(300)
	} else {
		q.speedtest_every = 300
	}

	var o Outputter
	db_section := cfg.Section("db")
	if db_section == nil {
		log.Fatal("exactly one [db] section must be present")
	}
	switch dbtype := db_section.Key("type").Value(); dbtype {
	case "sqlite":
		o = NewSqliteOutputter(q, db_section)
	case "postgres":
		o = NewPostgresOutputter(q, db_section)
	case "console":
		o = NewConsoleOutputter(q, db_section)
	default:
		log.Fatalf("database type %s not supported\n", dbtype)
	}
	q.o = o

	pingChan := make(chan pingResult)
	mtrChan := make(chan mtrResult)
	speedtestChan := make(chan speedtestResult)
	q.pingChan = pingChan
	q.mtrChan = mtrChan
	q.speedtestChan = speedtestChan

	// worker go
	go func() {
		for {
			select {
			case res := <-q.pingChan:
				o.PingResult(&res)
			case res := <-q.mtrChan:
				o.MtrResult(&res)
			case res := <-q.speedtestChan:
				o.SpeedtestResult(&res)
			}
		}
	}()

	ping_section := cfg.Section("ping")
	var interval, count uint
	if key := ping_section.Key("interval"); key != nil {
		interval = key.MustUint(180)
	} else {
		interval = 180
	}
	if key := ping_section.Key("count"); key != nil {
		count = key.MustUint(20)
	} else {
		count = 20
	}
	sections := ping_section.ChildSections()
	sort.Sort(SortableIniSections(sections))
	for _, section := range sections {
		if key := section.Key("interval"); key != nil {
			interval = key.MustUint(180)
		}
		if key := section.Key("count"); key != nil {
			count = key.MustUint(20)
		}
		if section.HasKey("target") {
			go ping(q, interval, section.Key("target").Value(), count)
		}
	}

	if cfg.Section("mtr").HasKey("target") {
		for _, target := range cfg.Section("mtr").Key("target").ValueWithShadows() {
			go mtr(mtrChan, q.mtr_every, target)
		}
	}

	if section := cfg.Section("speedtest"); section != nil {
		for _, typ := range section.Key("type").ValueWithShadows() {
			if typ == "speedtest-cli" {
				concurrent := section.Key("concurrent").MustUint(10)
				if concurrent > 10 {
					log.Fatalf("concurrent is automatically capped to 10")
				}
				go speedtest(q, speedtestChan, q.speedtest_every, concurrent)
			}
		}
	}

	select {}
}

func rename_prober(args []string) {
	log.Fatalf("Not implemented")
}

func start_db(q *Qualinternet) {
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	fmt.Fprintf(os.Stderr, "The following commands are available:\n")
	fmt.Fprint(os.Stderr, "  start CONFIGURATION_FILE\n")
	fmt.Fprint(os.Stderr, "  rename_prober FROM_NAME TO_NAME\n")
	os.Exit(1)
}

func main() {
	if len(os.Args) == 1 {
		usage()
	}
	command := os.Args[1]
	switch command {
	case "start":
		checkRequirements()
		start(os.Args)
	case "rename_prober":
		rename_prober(os.Args)
	default:
		log.Printf("Command %s is invalid\n\n", command)
		usage()
	}
}
