# qualinternet -- measure end user internet quality and provide simple but powerful reporting

The current status of this project is: work in progress and far from finished.

The main goal is to write a tool which continuously measures internet quality
and generate simple but powerful and easy to read reporting. Ideally, the
reports can be sent directly to your ISP if they, for instance, repeatedly fall
below the upload or download minima, or we detect packet loss, routing problems,
latency issues, unfair treatment of protocols (tcp vs. udp or ipv4 vs. ipv6).

# Goal
- gather information about the quality of the internet
- display it detailed and easy to understand
- keep data without rotating (only manually clean?)
- easily generate reports (e.g. pdf or text format in additionl to graphs)
- easily export data (as csv, json, etc.)

# What to monitor (brainstorming)
- ping (median, avg, max, min, standard deviation)
- ping with different payloads
- packetloss (median, avg, max, min, standard deviation)
- jitter
- download speed (through one single and several concurrent connections)
- upload speed
- routes/routing
- dns (both the plastebox as well as hosting a resolver)
- the local gateway and http(s) port
- tcp connections/second
- tls handshakes/second
- simulate services like https, openvpn, wireguard, tor
- some providers limit bandwidth/qos for specific ports or protocols
- with VPN and without VPN (combination of ports udp and tcp 1194, 51820, etc.)

## for every combination of
- ipv6
- ipv4
- tcp
- udp

## for different targets
- alexa top 100?
- servers at different, big providers

# how to monitor
- have a complete scan every 15 minutes (scattered, each scan runs every 15 minutes)

# hardware requirements
- preferrably none except for network connectivity
- >= gigabit ethernet connection to the local router

# Stuff to figure out
- make sure to reresolve DNS entries (they might change)
- how are nodes provisioned
